<?php
/**
 * ownCloud - storageusage
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Florian Kaiser <florian.kaiser@mpcdf.mpg.de>
 * @copyright Florian Kaiser 2015
 */

namespace OCA\StorageUsage\Controller;

use OCP\IRequest;
use OCP\IConfig;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;

use OCA\StorageUsage\Db\UsageDAO;

class PageController extends Controller {


	protected $usagedao;
	protected $config;


	public function __construct($AppName, IRequest $request, IConfig $config, UsageDAO $usagedao) {
		parent::__construct($AppName, $request);
		$this->config = $config;
		$this->usagedao = $usagedao;
	}


	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
	public function index() {
		//return new TemplateResponse('storageusage', 'main', $params);
		return $this->storageUsage();
	}

	/**
	 * @AdminRequired
	 * @NoCSRFRequired
	 */
	public function storageUsage() {
		$data = $this->usagedao->getUsage();

		$defaultQuota = $this->config->getAppValue('files', 'default_quota', 'none');

		for ($i=0; $i < count($data); ++$i) {
			$data[$i]['bytes'] = intval($data[$i]['bytes']);
			if ($data[$i]['quotastring'] == "none") {
				$data[$i]['quotavalue'] = INF;
			} else if ($data[$i]['quotastring'] == "default") {
				$data[$i]['quotastring'] = "default (" . $defaultQuota . ")";
				$data[$i]['quotavalue'] = \OCP\Util::computerFileSize($defaultQuota);
			} else {
				$data[$i]['quotavalue'] = \OCP\Util::computerFileSize($data[$i]['quotastring']);
			}
			// workaround for division by zero - 1B is good enough
			if ($data[$i]['quotavalue'] == 0) $data[$i]['quotavalue'] = 1;
		}

		$params = [ 'data' => $data ];
		return new TemplateResponse('storageusage', 'storageusage', $params);
	}

}