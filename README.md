# Storage Usage

![Screenshot](screenshot.png)

# Installation
Place this app in **owncloud/apps/**

This app is only meant for admins, so best restrict it to the admin group in the app settings.

Otherwise normal users will see it in their navigation but get an error when they try to access it.

Requires ownCloud 10 or later.

# Disclaimer
This app basically does SUMs over the oc_filecache table.

On our environment with ~1M rows, it takes a few seconds to load. 

Still, you might want to be careful if you have lots of files in your ownCloud.
