# TODO

 * Use existing ownCloud CSS classes if possible
   for consistent look & feel

 * Add index page with button, explaining what happens:
   "Click here if you really want to load this view, it make take a while"
 
 * Make table sortable, either server-side or at least with JavaScript

 * Create nicer iapp icon

 * Add diagrams


# Future ideas

 * Log historic data
