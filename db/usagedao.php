<?php

namespace OCA\StorageUsage\Db;

use OCP\IDBConnection;

class UsageDAO {
    private $db;

    public function __construct(IDBConnection $db) {
        $this->db = $db;
    }

    public function getUsage() {
	$dbPlatform = $this->db->getDatabasePlatform();
	$join_storage_expr = $dbPlatform->getConcatExpression('"home::"', '*PREFIX*accounts.user_id');

        $sql = 'SELECT *PREFIX*accounts.user_id AS username, *PREFIX*accounts.quota as quotastring, *PREFIX*filecache.size as bytes, sub.num as objects
                FROM *PREFIX*accounts
                LEFT JOIN *PREFIX*storages ON (*PREFIX*storages.id=' . $join_storage_expr . ')
                LEFT JOIN *PREFIX*filecache ON (*PREFIX*storages.numeric_id=*PREFIX*filecache.storage)
                LEFT JOIN (SELECT storage, COUNT(*) as num FROM *PREFIX*filecache GROUP BY storage) sub ON (sub.storage=*PREFIX*storages.numeric_id)
                WHERE *PREFIX*filecache.path="files"
                ORDER BY *PREFIX*filecache.size DESC;';

        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

	$stmt->closeCursor();

	return $result;

    }
}
