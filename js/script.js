/**
 * ownCloud - storageusage
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Florian Kaiser <florian.kaiser@mpcdf.mpg.de>
 * @copyright Florian Kaiser 2015
 */

(function ($, OC) {
	
	$(document).ready(function () {
		$(".storageusage-progress").each(function() {
		  $(this).progressbar({
		    value: parseInt( $(this).attr("storageusage-percent") )
		  });
		});
		$("table").tablesorter({
			theme: 'blue',
			textExtraction: function(node) {
				if ($(node).attr("value")) {
					return $(node).attr("value");
				} else {
					return $(node).text();
				}
			}
		});
	});

})(jQuery, OC);