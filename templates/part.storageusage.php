<div id="storageusage-wrapper">
<table id="storageusage">
<thead>
  <tr>
    <th>User</th>
    <th>Objects</th>
    <th>Space</th>
    <th>Quota</th>
    <th>% Used</th>
  <tr>
</thead>
<tbody>

<?php 
  $alt=false; 
  $total_objects = 0;
  $total_bytes = 0;
  $total_quota = 0;
?>
<?php 
  foreach($_['data'] as $row){ 
	  $total_objects += $row['objects'];
	  $total_bytes += $row['bytes'];
	  $total_quota += intval($row['quotavalue']);
?>
  <tr class="<?php if ($alt=!$alt) echo 'alt';?>">
    <td><?php p($row['username']); ?></td>
    <td><?php p($row['objects']); ?></td>
    <td value="<?php p($row['bytes']); ?>"><?php p($this->human_file_size($row['bytes'])); ?></td>
    <td value="<?php p($row['quotavalue']); ?>"><?php p($row['quotastring']); ?></td>
    <td value="<?php p((int) (100 * $row['bytes'] /  $row['quotavalue'])); ?>"><div class="storageusage-progress" storageusage-percent="<?php p((int) (100 * $row['bytes'] /  $row['quotavalue'])); ?>"></div></td>
  </tr>
<?php } ?>
</tbody>
<tfoot>
  <tr>
    <td>TOTAL</td>
    <td><?php p($total_objects); ?></td>
    <td value="<?php p($total_bytes); ?>"><?php p($this->human_file_size($total_bytes)); ?></td>
    <td value="<?php p($total_quota); ?>"><?php p($this->human_file_size($total_quota)); ?></td>
    <td>&nbsp;</td>
  </tr>
</tfoot>
</table>

</div>
