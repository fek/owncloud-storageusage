<div id="storageusage-wrapper">
<table id="storageusage">
<thead>
  <tr>
    <th>User</th>
    <th>Objects</th>
    <th>Space</th>
    <th>Quota</th>
    <th>% Used</th>
  <tr>
</thead>
<tbody>

<?php $alt=false; ?>
<?php foreach($_['data'] as $row){ ?>
  <tr class="<?php if ($alt=!$alt) echo 'alt';?>">
    <td><?php p($row['username']); ?></td>
    <td><?php p($row['objects']); ?></td>
    <td value="<?php p($row['bytes']); ?>"><?php p($this->human_file_size($row['bytes'])); ?></td>
    <td value="<?php p($row['quotavalue']); ?>"><?php p($row['quotastring']); ?></td>
    <td value="<?php p((int) ($row['bytes'] /  $row['quotavalue'])); ?>"><div class="storageusage-progress" storageusage-percent="<?php p((int) ($row['bytes'] /  $row['quotavalue'])); ?>"></div></td>
  </tr>
<?php } ?>

</tbody>
</table>

</div>