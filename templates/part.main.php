<div id="storageusage-wrapper">
<p>This app displays a table with information about the current storage usage for all users.</p>
<p>Collecting the information may take a while if you have many users and/or files.</p>
<p>This app should only be enabled for admin users.<p>
</div>
