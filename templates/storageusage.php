<?php
script('storageusage', 'jquery.tablesorter');
script('storageusage', 'script');
style('storageusage', 'style');
style('storageusage', 'tablesorter');
?>

<div id="app">
	<div id="app-navigation">
		<?php print_unescaped($this->inc('part.navigation')); ?>
		<?php print_unescaped($this->inc('part.settings')); ?>
	</div>

	<div id="app-content">
		<div id="app-content-wrapper">
			<?php print_unescaped($this->inc('part.storageusage')); ?>
		</div>
	</div>
</div>
