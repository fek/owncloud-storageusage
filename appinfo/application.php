<?php
namespace OCA\StorageUsage\AppInfo;

use OCP\AppFramework\App;

use OCA\StorageUsage\Controller\PageController;
use OCA\StorageUsage\Db\UsageDAO;

class Application extends App {
   public function __construct(array $urlParams=array()){
     parent::__construct('storageusage', $urlParams);

     $container = $this->getContainer();

     $container->registerService('UsageDAO', function($c){
	     return new UsageDAO(
		     $c->query('ServerContainer')->getDb()
	     );
     });

     $container->registerService('PageController', function($c){
       return new PageController(
         $c->query('AppName'),
	 $c->query('Request'),
	 $c->query('OCP\IConfig'),
	 $c->query('UsageDAO')
       );
     });

   }
}
